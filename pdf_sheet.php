<?php

include('functions.inc');
require('phppdf/phppdflib.class.php');
require('phppdf/template.class.php'); // Linien zeichnen


// generiert Seiten anhand der Datei und der Seitengroesse
function generate_page($buzzwordfile, $papersize) {

    global $pdf;
    global $seitenzahl;

	$pdf->set_default('margin', 20);


    // Buzzwords holen
    $buzzword_array = deserialize($buzzwordfile);

    // alle Seiten erstmal in einem Array ablegen - anschliessend
    // koennen sie alle ausgelesen werden
    $page = array();

    for ($i = 0; $i < $seitenzahl; $i++) {

         $buzzwords = array();
         get_buzzwords($buzzword_array[BUZZWORD], 25, &$buzzwords);


        switch (strtolower($papersize)) { // welche Groesse sollen die Blaetter haben?
            case "letter":
            case "a4":
            case "a5":
                $page[] = $pdf->new_page(strtolower($papersize));
                break;
            // Standard ist A4
            default:
                $page[] = $pdf->new_page("a4");
                break;
            }



        $param["height"] = 24; // Schriftgroesse
        $param["fillcolor"] = $pdf->get_color('#9f9f9f'); // Schriftfarbe
        $param["font"] = "Helvetica"; // Schriftart
        // Abhaengig von $papersize gestalten!
        $pdf->draw_text(200, 700, "Bullshit-Bingo!", $page[$i], $param);



        // Werte fuer die Buzzword-Tabelle generieren
        /*
         * Seitenaubau einer PDF-Seite
         *     __________
         *  ^  |        |
         *  |  |        |
         *  Y  |        |
         *  |  |        |
         *     |        |
         *     |        |
         *     ----------
         *        -X->
         *
         *  A4: 842 * 595
         */

        $xa = 50;
        $ya = 600;

        // Tabellenlinien zeichnen
	// $pdf->draw_line($x, $y, $firstpage, $p);


        $defaultparam["height"] = 13; // Schriftgroesse
        $defaultparam["fillcolor"] = $pdf->get_color('#000000'); // Schriftfarbe
        $defaultparam["font"] = "Helvetica"; // Schriftart

        reset($buzzwords); // Array-Counter zuruecksetzen
        // solange Buzzwords da sind
       foreach ($buzzwords as $word) {
            // schreibe Buzzword
            $param = $defaultparam;
            do {
                $param["height"]--;
            } while ($pdf->strlen("$word",$param) > 80);
            $pdf->draw_text($xa, $ya, $word, $page[$i], $param);

            $xa += 100;
            if ($xa >= 550) {
                $ya -= 30;
                $xa = 50;
            }
        }
    }



	// pruefen ob die Anzahl der Buzzwords eine Quadratzahl ist
	// Abhaengig von $papersize gestalten!
	// Tabelle bauen

	// dem Browser erklaeren, dass es sich hierbei um ein PDF handelt
	header("Content-Disposition: filename=pdf_sheet.pdf");
	header("Content-Type: application/pdf");
	$temp = $pdf->generate();
	header('Content-Length: ' . strlen($temp));

	return $temp;
}

// ueberprueft die angegebene Seitenzahl
function validate_pagecount($seitenzahl) {
    // zwischen 1 und 30
    if ($seitenzahl > 30 || $seitenzahl < 1) {
        echo "Nicht zwischen 1 und 30. <br>";
        return false;
    }
    return true;
}


// Seitenanzahl holen und nach int konvertieren
if (!isset($seitenzahl)) {
	echo "Seitenzahl bitte angeben!";
	return false;
}
$seitenzahl = intval($seitenzahl);

// Thema holen
$thema = $thema . ".xml";

// Eingabe der Seitenanzahl ueberpruefen
if (!validate_pagecount($seitenzahl)) {
    echo "Die Eingabe \"$temp\" ist keine gueltige Seitenanzahl! Bitte eine Seitenanzahl zwischen 1 und 30 eingeben. <br>";
    echo "<a href=\"javascript:history.back()\">Zur&uuml;ck</a>";
    return false;
} else {

// existiert die Datei?
if (!file_exists($thema)) {
    echo "Das gew&uuml;nschte Thema existiert z.Z. nicht. <br>";
    echo "<a href=\"javascript:history.back()\">Zur&uuml;ck</a><br>";
    echo "Thema war: $thema";
    return false;
}


    $pdf = new pdffile;


    echo (generate_page($thema, "a4"));

}

?>
