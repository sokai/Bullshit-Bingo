<?php

	function deserialize($filename) { // laed eine Buzzword-Datei

		if (!is_file($filename)) die("[FEHLER] Die Datei existiert nicht."); // die Datei existiert nicht
		$buzzword_array = array("BUZZWORD" => array(), "PRONUNCIATION" => array());

		$handle = fopen($filename,"r");

		$content = fread($handle, filesize($filename));
		$parser = xml_parser_create();
		xml_parse_into_struct($parser, $content, $values, $index); // Parse die Datei - in $values stehen alle Werte
		xml_parser_free($parser);

		for ($i = 0; $i < count($values); $i++) { // jedes Array-Feld durchgehen
			if ($values[$i][tag] == "NAME" && $values[$i][level] == "4" && $values[$i][type] == "complete") { // <Tag> == NAME, <level> == 4, <type> == complete
				array_push($buzzword_array[BUZZWORD],$values[$i][value]); // das jeweilige Buzzword in einem Array speichern
				array_push($buzzword_array[PRONUNCIATION],$values[$i + 3][value]); // entsprechende Schriftsprache im Array speichern - ist genau drei Index-Werte weiter als das Buzzword
			}

		}
		
		natcasesort($buzzword_array); // sortieren
		fclose($handle);

		return $buzzword_array;

	}

	function buzzword_exists($buzzword_array, $buzzword) { // prueft ob ein Wort in einem Array vom Buzzwords vorkommt
		for ($i = 0; $i < count($buzzword_array); $i++) {
			if (strnatcasecmp($buzzword, $buzzword_array[$i]) == 0)
				return true;
		}
		return false;
	}

	function get_buzzwords($buzzword_array, $count, &$buzzwords) { // holt eine bestimmte Anzahl von Buzzwords aus einem Array
        shuffle($buzzword_array);
		$buzzwords = array();
        $buzzwords = array_slice($buzzword_array,0,$count);
		// eventuell noch similar_text() einbauen
	}

?>
